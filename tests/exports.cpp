/*! exports.cpp */

#include <cmalloc/exports.h>

#include <gtest/gtest.h>

TEST(exports, macro)
{
#if defined(CMALLOC_API)
#  define VALUE 1
#else
#  define VALUE 0
#endif
    EXPECT_EQ(VALUE, 1);
#undef VALUE
}
