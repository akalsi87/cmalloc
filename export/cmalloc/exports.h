/*! exports.h */

#ifndef _CMALLOC_EXPORTS_H_
#define _CMALLOC_EXPORTS_H_

#if defined(USE_CMALLOC_STATIC)
#  define CMALLOC_API
#elif defined(_WIN32) && !defined(__GCC__)
#  ifdef BUILDING_CMALLOC_SHARED
#    define CMALLOC_API __declspec(dllexport)
#  else
#    define CMALLOC_API __declspec(dllimport)
#  endif
#  ifndef _CRT_SECURE_NO_WARNINGS
#    define _CRT_SECURE_NO_WARNINGS
#  endif
#else
#  ifdef BUILDING_CMALLOC_SHARED
#    define CMALLOC_API __attribute__ ((visibility ("default")))
#  else
#    define CMALLOC_API 
#  endif
#endif

#if defined(__cplusplus)
#  define CMALLOC_EXTERN_C extern "C"
#  define CMALLOC_C_API CMALLOC_EXTERN_C CMALLOC_API
#else
#  define CMALLOC_EXTERN_C
#  define CMALLOC_C_API CMALLOC_API
#endif

#endif/*_CMALLOC_EXPORTS_H_*/
