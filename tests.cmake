# CMakeLists.txt

# tests

set(test_dir ${PROJECT_SOURCE_DIR}/tests)

file(GLOB src_files ${test_dir}/*.cpp)

msg("Test files:")
foreach(f ${src_files})
  string(REPLACE "${PROJECT_SOURCE_DIR}/" "" f "${f}")
  msg("  ${f}")
endforeach()

enable_testing()
find_package(GTest REQUIRED)
find_package(benchmark CONFIG REQUIRED)

add_custom_target(tests)

foreach(f ${src_files})
  get_filename_component(tgt ${f} NAME)
  set(tgt ${tgt}.test)
  add_executable(${tgt} ${f})
  target_include_directories(
    ${tgt}
    PRIVATE ${PROJECT_SOURCE_DIR}/export)
  target_link_libraries(
    ${tgt}
    cmalloc
    GTest::GTest
    GTest::Main
    benchmark::benchmark)
  add_test(NAME ${tgt} COMMAND $<TARGET_FILE:${tgt}>)
  add_custom_target(
    ${tgt}.run
    DEPENDS ${tgt}
    COMMAND $<TARGET_FILE:${tgt}>)
  add_dependencies(tests ${tgt}.run)
endforeach()
